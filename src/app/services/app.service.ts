import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  get = (key:string):Observable<Response | any> => {
    let params = new HttpParams();
    params = params.append('key', key);
    return this.http.get<Response | any>(`${environment.api}/hash`, {params});
  }
  
  confirmarInvitacion = (data:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/confirmar-invitacion`, data);
  }
}
