import { Component, Input } from '@angular/core';
import * as dayjs from 'dayjs';
import * as duration from 'dayjs/plugin/duration';
dayjs.extend(duration);

@Component({
  selector: 'app-count-down',
  templateUrl: './count-down.component.html',
  styleUrls: ['./count-down.component.scss']
})
export class CountDownComponent {

  @Input() set fecha(fecha:string | undefined | null){
    if(typeof fecha === 'string'){

      this.fechaObjetivo = dayjs(fecha);
      this.intervalId = setInterval(() => {
        this.actualizarContador();
      }, 1000);

    }
  }

  @Input() set bgColor(bgColor:string | undefined | null){
    if(typeof bgColor === 'string'){
      this.cdBgColor = bgColor;
    }
  }

  @Input() set textColor(textColor:string | undefined | null){
    if(typeof textColor === 'string'){
      this.cdTextColor = textColor;
    }
  }

  private fechaObjetivo: any = null;
  private intervalId: any;
  public esHoy: boolean = false;
  public days: number = 0;
  public hours: number = 0;
  public minutes: number = 0;
  public seconds: number = 0;
  public cdBgColor: string = 'white';
  public cdTextColor: string = 'black';


  private actualizarContador() {
    
    const diff = this.fechaObjetivo.diff(dayjs(), 'second');
    if (diff <= 0) {
      this.esHoy = true;
      clearInterval(this.intervalId);
    } else {
      const duration = dayjs.duration(diff, 'second');
      this.days = duration.days();
      this.hours = duration.hours();
      this.minutes = duration.minutes();
      this.seconds = duration.seconds();
    }
    
  }

}
