import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from  '@angular/common/http';
import { CountDownComponent } from './count-down/count-down.component';
import { DatePipe } from '@angular/common';
import {
  NgxUiLoaderModule,
  NgxUiLoaderConfig
} from "ngx-ui-loader";
import { NgsRevealModule } from 'ngx-scrollreveal';

// Primeng
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  "bgsColor": "#f08080",
  "bgsOpacity": 0.4,
  "bgsPosition": "bottom-right",
  "bgsSize": 40,
  "bgsType": "double-bounce",
  "blur": 5,
  "delay": 0,
  "fastFadeOut": true,
  "fgsColor": "#f08080",
  "fgsPosition": "center-center",
  "fgsSize": 60,
  "fgsType": "double-bounce",
  "gap": 24,
  "logoPosition": "center-center",
  "logoSize": 120,
  "logoUrl": "",
  "masterLoaderId": "master",
  "overlayBorderRadius": "0",
  "overlayColor": "rgba(255,194,229,0.15)",
  // "overlayColor": "rgba(255,255,255, 0.75)",
  "pbColor": "#f08080",
  "pbDirection": "ltr",
  "pbThickness": 3,
  "hasProgressBar": false,
  "text": "",
  "textColor": "#FFFFFF",
  "textPosition": "center-center",
  "maxTime": -1,
  "minTime": 300
}

@NgModule({
  declarations: [
    AppComponent,
    CountDownComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgsRevealModule,
    //Primeng
    ButtonModule,
    DialogModule,
    AppRoutingModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
  ],
  providers: [
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
