import { Component, OnInit } from '@angular/core';
import { LocalService } from './services/local.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import { AppService } from './services/app.service';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as dayjs from 'dayjs'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  data: any = null;
  images: string[] = [];
  invitaciones: any[] = [];
  showConfirmar: boolean = false;
  key: string = '';
  confirmado : boolean = false;
  showDownCounter: boolean = true;
  invitado: string = '';

  constructor(private localService: LocalService, private title: Title, 
    private appService: AppService, private route: ActivatedRoute, private loader: NgxUiLoaderService){
      this.route.queryParams.subscribe((data:any) => {
        if('key' in data){
          this.key = data.key;
          this.getInvitacion();
        }
      })
  }

  ngOnInit(): void {
    this.getData();
  }

  getData = () => {
    this.localService.getData().subscribe({
      next: (response) => {
        this.data = response;
        this.images = response.album.images;
        if(!dayjs().isBefore(dayjs(this.data.qDate))){
          this.showDownCounter = false;
        }
        this.title.setTitle(this.data.appTitle);
      },
      error: (err:HttpErrorResponse) => {
        this.data = null;
      }
    })
  }

  openMap = () => {
    window.open(this.data?.inv.ubiMapa, "_blank");
  }

  getInvitacion = () => {
    this.loader.start();
    this.appService.get(this.key).subscribe({
      next: (response:any) => {
        this.invitaciones = response.result;
        for(let invi of this.invitaciones){
          if('asistencia' in invi && invi.asistencia == 1){
            this.confirmado = true;
          }

          if(this.invitado === ''){
            this.invitado = invi.nombre;
          }

        }
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.loader.stop();
        this.invitaciones = [];
      }
    })
  }
  
  confirmarInvitacion = () => {
    this.loader.start();
    this.appService.confirmarInvitacion({ hash : this.key }).subscribe({
      next: (response:any) => {
        this.loader.stop();
        this.confirmado = true;
        this.showConfirmar = false;
      },
      error: (err:HttpErrorResponse) => {
        this.loader.stop();
      }
    })
  }

}
